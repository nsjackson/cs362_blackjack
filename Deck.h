/*
 * Deck.h
 *
 *  Created on: Jan 15, 2015
 *      Author: Nic Jackson
 */
#ifndef DECK_H_
#define DECK_H_

#include <vector>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Card.h"

using namespace std;

class Deck {

public:
	Deck(int numDecks);
	~Deck();
	void shuffleDeck();
	Card * dealOne();
	void printDeck();
	int getSize();
	int getNumDealt();
	Card * getTop();
	int testDealDeck();

private:
	void fillDeck(int deckCount);
	int _maxSize;
	int _numEach;
	vector<Card *> deck;
	vector<Card *> dealt;

};

#endif /* DECK_H_ */
