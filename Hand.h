/*
 * Hand.h
 *
 *  Created on: Jan 28, 2015
 *      Author: Nic Jackson
 */

#ifndef HAND_H_
#define HAND_H_
#include <vector>

#include "Card.h"
#include "Deck.h"
using std::vector;

class Hand {
public:
	Hand(Deck *curDeck);
	virtual ~Hand();
	void printHand();
	int getValue();
	int draw();
	void testDraw();
	void clearHand();
	int getSize();

private:
	vector<Card *> hand;
	Deck *deck;
	int _value;
	int calcValue();
	int testCalcValue();

};

#endif /* HAND_H_ */
