/*
 * Card.h
 *
 *  Created on: Jan 8, 2015
 *      Author: Nic Jackson
 */
#ifndef CARD_H_
#define CARD_H_

#include <iostream>
#include <cstdlib>
class Card {

public:
	Card(const char suit, const int rank);
	int getValue();
	std::string getSuit();
	std::string getRank();

	int setValue();
	void flipAce();

	void printCard(int mode);

private:

	int _value;
	char _suit;
	int _rank;
	char mapRank(int rank);

};
#endif /* CARD_H_ */
