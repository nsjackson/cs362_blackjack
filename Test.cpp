/*
 * test.cpp
 *
 *  Created on: Jan 15, 2015
 *      Author: Nic Jackson
 */
#include "Deck.h"
#include "Card.h"
#include <cstdlib>
#include "Hand.h"
#include "Player.h"

#define STAT_TEST 10000
#define CALC_STAT /STAT_TEST *100

int main(int argc, char **argv) {
	if (argc == 2) {
		int deckTest = 0;
		int testVal = atoi(argv[1]);
		Deck *testDeck = new Deck(testVal);
//		testDeck->printDeck();
		if (testDeck->getSize() != 52 * testVal) {
			cout << testVal << " deck size: FAIL" << endl;
		} else {
			cout << testVal << " deck size: PASS" << endl;
			deckTest = 1;
		}
		Card * first = testDeck->getTop();
		testDeck->shuffleDeck();
		Card * second = testDeck->getTop();
		if (first->getRank() != second->getRank() || first->getSuit() != second->getSuit()) {
			cout << testVal << " deck shuffle: PASS" << endl;
			deckTest = 1;

		} else {
			cout << testVal << " deck  shuffle: FAIL" << endl;
		}
		if (testDeck->testDealDeck() == testDeck->getNumDealt()) {
			cout << testVal << " deck  deal: PASS" << endl;
			deckTest = 1;

		} else {
			cout << testVal << " deck  deal: FAIL" << endl;
		}
		cout << endl;
		if (deckTest == 0) {
			cout << "One or more Deck tests failed, exiting." << endl;
			delete (testDeck);
			exit(1);
		} else if (deckTest == 1) {
			cout << "All Deck tests passed, moving on to Hand tests." << endl;
			cout << endl;
			delete (testDeck);
		}
		testDeck = new Deck(testVal);
		Hand *testHand = new Hand(testDeck);
		for (int i = 0; i < 1; i++) {
			testDeck->shuffleDeck();
		}
		for (int i = 0; i < testVal * 52; i++) {
			testHand->draw();
		}
		cout << endl;
		cout << "Deck: " << testDeck->getSize() << endl;
		cout << "Dealt: " << testDeck->getNumDealt() << endl;
		delete (testHand);
		delete (testDeck);

		cout << "Running statistics..." << endl;
		double playerBlackjack = 0;
		double dealerBlackjack = 0;
		double dealerStand = 0;
		double playerBust = 0;
		double dealerBust = 0;
		double playerHigh = 0;
		double dealerHigh = 0;
		double pFiveCardCharlie = 0;
		double dFiveCardCharlie = 0;

		for (int i = 0; i < STAT_TEST; i++) {
			testDeck = new Deck(testVal);
			testHand = new Hand(testDeck);
			Hand *testDealerHand = new Hand(testDeck);
			testDeck->shuffleDeck();
			for (int j = 0; j < 2; j++) {
				testHand->testDraw();
				testDealerHand->testDraw();
			}
			if (testHand->getValue() == 21) {
				playerBlackjack++;
			} else if (testHand->getValue() > 21) {
				playerBust++;
			}
			if (testDealerHand->getValue() == 21) {
				dealerBlackjack++;
			} else if (testDealerHand->getValue() >= 17) {
				dealerStand++;
			} else if (testDealerHand->getValue() > 21) {
				dealerBust++;
			}
			if (testHand->getValue() > testDealerHand->getValue()) {
				playerHigh++;
			} else if (testHand->getValue() < testDealerHand->getValue()) {
				dealerHigh++;
			}
			for (int h = 0; h < 3; h++) {
				testHand->testDraw();
				testDealerHand->testDraw();
			}
			if (testHand->getValue() <= 21) {
				pFiveCardCharlie++;
			}
			if (testDealerHand->getValue() <= 21) {
				dFiveCardCharlie++;
			}
			delete (testHand);
			delete (testDealerHand);
			delete (testDeck);
		}
		double pBlackjack = (playerBlackjack)CALC_STAT;
		double dBlackjack = (dealerBlackjack)CALC_STAT;
		double pBust = (playerBust)CALC_STAT;
		double dBust = (dealerBust)CALC_STAT;
		double dStand = (dealerStand)CALC_STAT;
		double pHigh = (playerHigh)CALC_STAT;
		double dHigh = (dealerHigh)CALC_STAT;
		double pFive = (pFiveCardCharlie)CALC_STAT;
		double dFive = (dFiveCardCharlie)CALC_STAT;

		cout << "Over " << STAT_TEST << " iterations with " << testVal
				<< " decks, the following statistics apply." << endl;
		cout << "=====Initial Deal=====" << endl;
		cout << "=====PLAYER=====" << endl;
		cout << pBlackjack << "% chance of getting a blackjack." << endl;
		cout << pBust << "% chance of busting." << endl;
		cout << pHigh << "% chance of scoring higher than the dealer." << endl;
		cout << "=====DEALER=====" << endl;
		cout << dBlackjack << "% chance of getting a blackjack." << endl;
		cout << dBust << "% chance of busting." << endl;
		cout << dHigh << "% chance of scoring higher than the player." << endl;
		cout << dStand << "% chance of having to stand." << endl;
		cout << "=====Deal To Five Cards=====" << endl;
		cout << "=====PLAYER=====" << endl;
		cout << pFive << "% chance of getting five cards without busting." << endl;
		cout << "=====DEALER=====" << endl;
		cout << dFive << "% chance of getting five cards without busting." << endl;

		cout << "All Hand tests and statistics complete, moving on to Player tests and statistics."
				<< endl;
		Deck *playerTestDeck = new Deck(testVal);
		Player *testPlayer1 = new Player(playerTestDeck, 15, "Player1");

		testPlayer1->hit();
		testPlayer1->hit();
		if (testPlayer1->getHandSize() == 2) {
			cout << "Hit test: PASSED" << endl;
		} else {
			cout << "Hit test: FAILED" << endl;
		}
		testPlayer1->hit();
		if (testPlayer1->getHandValue() > 21) {
			if (testPlayer1->getBust()) {
				cout << "Bust test: PASSED" << endl;
			} else {
				cout << "Bust test: FAILED" << endl;
			}
		}
		if (testPlayer1->getHandValue() >= 15) {
			if (testPlayer1->getStay()) {
				cout << "Stay test: PASSED" << endl;
			} else {
				cout << "Stay test: FAILED" << endl;
			}

		}
		if (testPlayer1->getName() == "Player1") {
			cout << "Name test: PASSED" << endl;
		} else {
			cout << "Name test: PASSED" << endl;
		}
		cout << "Testing printHand()..." << endl;
		testPlayer1->printHand();

		cout << "Clearing hand..." << endl;
		testPlayer1->clearHand();
		if (testPlayer1->getHandSize() == 0) {
			cout << "Clear Hand test: PASSED" << endl;
		} else {
			cout << "Clear Hand test: FAILED" << endl;

		}
		delete (testPlayer1);
		delete (playerTestDeck);
		cout << "Single Player tests finished, running multiple player tests and statistics..."
				<< endl;

		double player1Blackjack = 0;
		double player2Blackjack = 0;
		double player1Stand = 0;
		double player2Stand = 0;
		double player1Bust = 0;
		double player2Bust = 0;
		double player1High = 0;
		double player2High = 0;
		double p1FiveCardCharlie = 0;
		double p2FiveCardCharlie = 0;

		for (int i = 0; i < STAT_TEST; i++) {
			Deck *playerTestDeck = new Deck(testVal);
			Player *testPlayer1 = new Player(playerTestDeck, 17, "TestPlayer1");
			Player *testPlayer2 = new Player(playerTestDeck, 16, "TestPlayer2");
			playerTestDeck->shuffleDeck();
			for (int j = 0; j < 2; j++) {
				testPlayer1->hit();
				testPlayer2->hit();
			}
			if (testPlayer1->getHandValue() == 21) {
				player1Blackjack++;
			} else if (testPlayer1->getStay()) {
				player1Stand++;
			} else if (testPlayer1->getBust()) {
				player1Bust++;
			}
			if (testPlayer2->getHandValue() == 21) {
				player2Blackjack++;
			} else if (testPlayer2->getStay()) {
				player2Stand++;
			} else if (testPlayer2->getBust()) {
				player2Bust++;
			}
			if (testPlayer1->getHandValue() > testPlayer2->getHandValue()) {
				player1High++;
			} else if (testPlayer1->getHandValue() < testPlayer2->getHandValue()) {
				player2High++;
			}
			for (int h = 0; h < 3; h++) {
				testPlayer1->hit();
				testPlayer2->hit();
			}
			if (testPlayer1->getHandValue() <= 21 && testPlayer1->getHandSize() == 5) {
				p1FiveCardCharlie++;
			}
			if (testPlayer2->getHandValue() <= 21 && testPlayer2->getHandSize() == 5) {
				p2FiveCardCharlie++;
			}
			delete (testPlayer1);
			delete (testPlayer2);
			delete (playerTestDeck);
		}
		double p1Blackjack = (player1Blackjack)CALC_STAT;
		double p2Blackjack = (player2Blackjack)CALC_STAT;
		double p1Bust = (player1Bust)CALC_STAT;
		double p2Bust = (player2Bust)CALC_STAT;
		double p1Stand = (player1Stand)CALC_STAT;
		double p2Stand = (player2Stand)CALC_STAT;
		double p1High = (player1High)CALC_STAT;
		double p2High = (player2High)CALC_STAT;
		double p1Five = (p1FiveCardCharlie)CALC_STAT;
		double p2Five = (p2FiveCardCharlie)CALC_STAT;

		cout << "Over " << STAT_TEST << " iterations with " << testVal
				<< " decks, the following statistics apply." << endl;
		cout << "=====Initial Deal=====" << endl;
		cout << "=====PLAYER 1=====" << endl;
		cout << "=====Must Stand on 17=====" << endl;
		cout << p1Blackjack << "% chance of getting a blackjack." << endl;
		cout << p1Bust << "% chance of busting." << endl;
		cout << p1High << "% chance of scoring higher than player 2." << endl;
		cout << p1Stand << "% chance of having to stand." << endl;
		cout << "=====PLAYER 2=====" << endl;
		cout << "=====Must Stand on 16=====" << endl;
		cout << p2Blackjack << "% chance of getting a blackjack." << endl;
		cout << p2Bust << "% chance of busting." << endl;
		cout << p2High << "% chance of scoring higher than player 1." << endl;
		cout << p2Stand << "% chance of having to stand." << endl;
		cout << "=====Deal To Five Cards=====" << endl;
		cout << "=====PLAYER 1=====" << endl;
		cout << "=====Must Stand on 17=====" << endl;
		cout << p1Five << "% chance of getting five cards without busting." << endl;
		cout << "=====PLAYER 2=====" << endl;
		cout << "=====Must Stand on 16=====" << endl;
		cout << p2Five << "% chance of getting five cards without busting." << endl;

	}

}

