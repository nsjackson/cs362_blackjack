CC = g++
CFLAGS = -Wall -g
TARGET = blackjack

all : ${TARGET}

blackjack : Test.o Deck.o Card.o Hand.o Player.o
	${CC} ${CFLAGS} Test.o Deck.o Card.o Hand.o Player.o -o blackjack
	
Card.o : Card.cpp Card.h
	${CC} ${CFLAGS} -c Card.cpp

Deck.o : Deck.cpp Deck.h Card.h
	${CC} ${CFLAGS} -c Deck.cpp

Hand.o : Hand.cpp Hand.h Card.h Deck.h
	${CC} ${CFLAGS} -c Hand.cpp

Player.o : Player.cpp Player.h Hand.h Deck.h
	${CC} ${CFLAGS} -c Player.cpp

Test.o : Test.cpp Deck.h Card.h
	${CC} ${CFLAGS} -c Test.cpp
	
clean : 
	@rm -f ${TARGET} *.o