/*
 * Hand.cpp
 *
 *  Created on: Jan 28, 2015
 *      Author: Nic Jackson
 */

#include "Hand.h"

Hand::Hand(Deck *curDeck) {
	deck = curDeck;
	hand.reserve(5);
	_value = 0;
}

Hand::~Hand() {
	clearHand();
}

void Hand::printHand() {
	vector<Card *>::iterator it;
	for (it = hand.begin(); it != hand.end(); it++) {
		cout << "[";
		(*it)->printCard(0);
		cout << "]" << endl;
	}
	cout << "Value: " << _value << endl;
}

int Hand::getValue() {
	return _value;
}

int Hand::draw() {
	int retVal = 0;
	if (hand.size() != 5 && _value <= 21) {
		hand.push_back(deck->dealOne());
		int tempVal = calcValue();
		if (tempVal < 0) {
			if (tempVal == -1) {
				printHand();
				cout << "BUST! Try Again" << endl;
				cout << endl;
				retVal = -1;
			} else if (tempVal == -2) {
				printHand();
				if (hand.size() == 2) {
					cout << "BLACKJACK!" << endl;
				} else if (hand.size() == 5) {
					cout << "Five Card Charlie!" << endl;
				} else {
					cout << "21, You Win!" << endl;
				}
				cout << endl;
				retVal = 1;
			}
		} else {
			_value = calcValue();
			printHand();
			if (hand.size() == 5) {
				cout << "Five Card Charlie!" << endl;
				cout << endl;
				retVal = 1;
			}
		}
	}
	return retVal;
}

void Hand::clearHand() {
	hand.clear();
	_value = 0;
}

void Hand::testDraw() {
	hand.push_back(deck->dealOne());
	_value = testCalcValue();
}

int Hand::calcValue() {
	vector<Card *>::iterator it;
	vector<Card *>::iterator it2;

	int retVal = 0;
	for (it = hand.begin(); it != hand.end(); it++) {
		retVal += (*it)->getValue();
	}
	_value = retVal;
	if (retVal > 21) {
		for (it = hand.begin(); it != hand.end(); it++) {
			if ((*it)->getRank() == "Ace") {
				(*it)->flipAce();
				retVal = 0;
				for (it2 = hand.begin(); it2 != hand.end(); it2++) {
					retVal += (*it2)->getValue();
				}
			}
		}
	}
	if (retVal > 21) {
		retVal = -1;
	}
	if (retVal == 21) {
		_value = retVal;
		retVal = -2;
	}
	return retVal;
}

int Hand::getSize() {
	return hand.size();
}

int Hand::testCalcValue() {
	vector<Card *>::iterator it;
	vector<Card *>::iterator it2;

	int retVal = 0;
	for (it = hand.begin(); it != hand.end(); it++) {
		retVal += (*it)->getValue();
	}
	if (retVal > 21) {
		for (it = hand.begin(); it != hand.end(); it++) {
			if ((*it)->getRank() == "Ace") {
				(*it)->flipAce();
				retVal = 0;
				for (it2 = hand.begin(); it2 != hand.end(); it2++) {
					retVal += (*it2)->getValue();
				}
			}
		}
	}
	return retVal;
}
