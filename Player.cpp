/*
 * Player.cpp
 *
 *  Created on: Feb 9, 2015
 *      Author: student
 */

#include "Player.h"

void Player::init(int mode, Deck *deck) {
	switch (mode) {
	case 0:
		_stayVal = 17;
	case 1:
		_name = "TestPlayer";
	case 2:
		_deck = deck;
		_hand = new Hand(_deck);
		_stay = false;
		_bust = false;
		break;
	}

}
Player::Player(Deck *deck) {
	init(0, deck);

}

Player::Player(Deck *deck, int rules) {
	init(1, deck);
	_stayVal = rules;
}

Player::Player(Deck *deck, int rules, string name) {
	init(2, deck);
	_name = name;
	_stayVal = rules;
}
Player::~Player() {
	clearHand();
	delete (_hand);
}

string Player::getName() {
	return _name;
}

bool Player::getBust() {
	return _bust;
}

bool Player::getStay() {
	return _stay;
}

void Player::hit() {
	if (_stay || _bust) {
		cout << getName() << " cannot hit, skipping..." << endl;
	} else {
		switch (_hand->draw()) {
		case -1:
			_bust = true;
			break;
		case 1:
			_stay = true;
			cout << getName() << " has a winning hand!" << endl;
			break;
		default:
			cout << getName() << " hit successfully" << endl;
			break;
		}
		printHand();
	}
}

void Player::stay() {
	_stay = true;
}

void Player::printHand() {
	cout << "Player: " << getName() << endl;
	_hand->printHand();
}

void Player::clearHand() {
	_hand->clearHand();
}

void Player::autoPlay() {
	if (getHandValue() >= _stayVal) {
		stay();
	} else {
		hit();
	}
}

int Player::getHandSize() {
	return _hand->getSize();
}

int Player::getHandValue() {
	return _hand->getValue();
}

