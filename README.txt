Card.h describes the  functions the Card object provides.
Card.cpp defines the functionality of the Card object.
Deck.h describes the functions the Deck object provides.
Deck.cpp defines the functionality of the Deck object.
Hand.h describes the functions the Hand object provides.
Hand.cpp defines the functionality of the Hand object.
Player.h describes the functions the Player object provides.
Player.cpp defines the functionality of the Player object.
Test.cpp provides an unit,integration and system testing suite

To compile and run all tests, execute these commands:

	chmod u+x run_tests.sh
	./run_tests.sh
	
This script will compile the program, run the tests for a deck count of 1, 2, 4, 6, 8, 9 decks.
The last deck count will cause a failure and exit the program.
Each test run will be output into a seperate file so as to allow easier viewing of Hand results and statistics.
After running these tests, the script will clean up the executable and object files.
The results of each test will be in a seperate file named testOutput[deckCount].txt.
Statistics will be at the very bottom of the Hand and Player tests, sorry...I need to split those tests up a bit.