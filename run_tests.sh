#1/bin/bash

echo "Building program..."
make

echo "Running all tests..."

for x in 1 2 4 6 8 9; do
	./blackjack "$x" >> testOutput"$x".txt
done

echo "Cleaning up program and object files..."
make clean

echo "Tests complete"
