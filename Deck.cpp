/*
 * Deck.cpp
 *
 *  Created on: Jan 15, 2015
 *      Author: Nic Jackson
 */

#include "Deck.h"
using namespace std;

int Deck::getNumDealt() {
	return dealt.size();
}

void Deck::fillDeck(int deckCount) {
	const char suits[] = { 'S', 'H', 'D', 'C' };
	const int ranks[] = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };

	for (int count = 0; count < deckCount; count++) {
		for (int cardSuit = 0; cardSuit < 4; cardSuit++) {
			for (int cardRank = 0; cardRank < 13; cardRank++) {
				int r = ranks[cardRank];
				int s = suits[cardSuit];
				Card * tmpCard = new Card(s, r);
				deck.push_back(tmpCard);
			}
		}
	}
}
Deck::~Deck() {
	for (unsigned int i = 0; i < dealt.size(); i++) {
		delete (dealt[i]);
		dealt.pop_back();
	}
}

Deck::Deck(int numDecks) {
	numDecks = (int) numDecks;
	if (numDecks == 1 || numDecks == 2 || numDecks == 4 || numDecks == 6 || numDecks == 8) {
		_maxSize = numDecks * 52;
		_numEach = numDecks;
	} else {
		cout << "ERROR: Choose between 1,2,4,6, or 8 decks." << endl;
		exit(1);
	}
//deck = deck(_size);
	deck.reserve(_maxSize);
	dealt.reserve(_maxSize);
	fillDeck(numDecks);
}
void Deck::shuffleDeck() {
	random_shuffle(deck.begin(), deck.end());
}
Card * Deck::getTop() {
	Card *topCard;
	topCard = deck.back();

	return topCard;
}
Card * Deck::dealOne() {
	Card * top = getTop();
//	if (top->getRank() == "Ace") {
	//	top->flipAce();
	//}
//	top->printCard();
	dealt.push_back(top);
	deck.pop_back();
	return top;
}
int Deck::testDealDeck() {
	int dealtCards = 0;
	for (int i = 0; i < _maxSize; i++) {
		dealOne();
//		deck.pop_back();
		dealtCards++;
	}
	return dealtCards;
}
int Deck::getSize() {
	return deck.size();
}
void Deck::printDeck() {
	cout << "Deck Size: " << _maxSize << endl;
	for (int card = 0; card < _maxSize; card++) {
		deck[card]->printCard(1);
	}
}
