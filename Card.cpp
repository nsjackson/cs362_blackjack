/*
 * Card.cpp
 *
 *  Created on: Jan 8, 2015
 *      Author: Nic Jackson
 */

#include "Card.h"

using namespace std;

char Card::mapRank(int rank) {
	char _retVal;
	if (rank < 10) {
		_retVal = (char) rank;
	} else if (rank == 10) {
		_retVal = 'T';
	} else if (rank == 11) {
		_retVal = 'J';
	} else if (rank == 12) {
		_retVal = 'Q';
	} else if (rank == 13) {
		_retVal = 'K';
	} else if (rank == 14) {
		_retVal = 'A';
	}
	return _retVal;
}

int Card::getValue() {
	return _value;
}

string Card::getSuit() {
	string outSuit;
	switch (_suit) {
	case 'S':
		outSuit = "Spades";
		break;
	case 'H':
		outSuit = "Hearts";
		break;
	case 'D':
		outSuit = "Diamonds";
		break;
	case 'C':
		outSuit = "Clubs";
		break;
	}
	return outSuit;
}

string Card::getRank() {
	string outRank;
	switch (_rank) {
	case 2:
		outRank = "Two";
		break;
	case 3:
		outRank = "Three";
		break;
	case 4:
		outRank = "Four";
		break;
	case 5:
		outRank = "Five";
		break;
	case 6:
		outRank = "Six";
		break;
	case 7:
		outRank = "Seven";
		break;
	case 8:
		outRank = "Eight";
		break;
	case 9:
		outRank = "Nine";
		break;
	case 'T':
		outRank = "Ten";
		break;
	case 'J':
		outRank = "Jack";
		break;
	case 'Q':
		outRank = "Queen";
		break;
	case 'K':
		outRank = "King";
		break;
	case 'A':
		outRank = "Ace";
		break;
	}
	return outRank;
}

void Card::flipAce() {


	_value = 1;
}

int Card::setValue() {
	int _retVal;
	if (_rank > 1 && _rank < 10) {
		_retVal = _rank;
	} else if (_rank == 'T' || _rank == 'J' || _rank == 'Q' || _rank == 'K') {
		_retVal = 10;
	} else if (_rank == 'A') {
		_retVal = 11;
	}
	return _retVal;
}
Card::Card(char suit, int rank) {
	_suit = suit;
	_rank = mapRank(rank);
	_value = setValue();
}

void Card::printCard(int mode) {
	cout << getRank() << " of " << getSuit();
	if (mode == 1) {
		cout << " Value: " << getValue() << endl;
	} else if (mode == 0) {
		return;
	} else {
		cout << endl;
	}
}
