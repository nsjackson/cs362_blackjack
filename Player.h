/*
 * Player.h
 *
 *  Created on: Feb 9, 2015
 *      Author: Nic Jackson
 */

#ifndef PLAYER_H_
#define PLAYER_H_
#include <iosfwd>

#include "Deck.h"
#include "Hand.h"
using std::string;

class Player {
public:
	Player(Deck *deck);
	Player(Deck *deck, int rules);
	Player(Deck *deck, int rules, string name);
	virtual ~Player();
	string getName();
	bool getBust();
	bool getStay();
	void hit();
	void stay();
	void printHand();
	void clearHand();
	int getHandSize();
	int getHandValue();
	void autoPlay();

private:
	bool _stay;
	bool _bust;
	int _stayVal;
	string _name;
	Deck *_deck;
	Hand *_hand;
	void init(int mode, Deck *deck);

};

#endif /* PLAYER_H_ */
